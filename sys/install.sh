#!/usr/bin/env sh

if [[ $EUID -ne 0 ]]; then
   echo "Elevate privileges to modify the system files"
   exit 1
fi

cp ../pootie.py /usr/local/bin/pootie

mkdir -p /usr/local/lib/pootie
cp ../lib/*.py /usr/local/lib/pootie

mkdir -p /usr/local/lib/systemd/system
cp ../sys/service /usr/local/lib/systemd/system/pootie.service

