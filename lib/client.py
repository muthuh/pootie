def client(logger, server_ip, port_no):

    from lib.graphics import Circle, GraphWin, GraphicsError, Image, Point
    from lib.game import Ball, Spot, Path, Player
    from lib.rc import user
    import shelve
    from twisted.internet.protocol import Protocol, Factory
    from twisted.internet import reactor
    from twisted.internet.task import LoopingCall
    from twisted.internet.threads import deferToThread

    class protocol_instance(Protocol):

        def __init__(self, factory):

            self.factory = factory
            self.user = user
            user_temp = input('INPUT username? [{}] '.format(self.user))
            if user_temp != '':
                self.user = user_temp
            del user_temp
            self.opponent = None
            self.users = []

            points_coords_shelve = shelve.open("data/points_coords.db")
            self.points_coordinates_x = points_coords_shelve['points_x-coords']
            self.points_coordinates_y = points_coords_shelve['points_y-coords']
            points_coords_shelve.close()
            border_spots_shelve = shelve.open("data/border_spots.db")
            self.border_spots = border_spots_shelve['border_points']
            border_spots_shelve.close()

        def connectionMade(self):

            self.player = Player(self.user, None, 'green')
            self.transport.write(self.user.encode())
            logger.debug('\'{}\' --> (\'{}\', {})'.format
                         (self.user, self.transport.getPeer().host,
                          self.transport.getPeer().port))

            self.defered_input = deferToThread(self.parse_input)
            LoopingCall(self.process_input).start(4)

        def parse_input(self):

            while True:

                self.input = input()
                logger.debug('input \'{}\''.format(self.input))

        def process_input(self):

            try:

                if self.input in self.users and not self.opponent:

                    self.transport.write(self.input.encode())
                    logger.debug('\'{}\' --> (\'{}\', {})'.format
                                 (self.input, self.transport.getPeer().host,
                                  self.transport.getPeer().port))
                    logger.info('invite \'{}\''.format(self.input))
                    self.opponent = self.input
                    self.player.acting = True
                    logger.debug('player.acting --> \'True\''.format
                                 (self.input))

                else:

                    logger.warn('failed parsing \'{}\''.format(self.input))

                del self.input

            except AttributeError:

                pass

        def dataReceived(self, data):

            logger.debug('(\'{}\', {}) --> \'{}\''.format
                         (self.transport.getPeer().host,
                          self.transport.getPeer().port, data))
            this_data = data.decode().split()

            if self.opponent:

                try:

                    self.move_ball(float(this_data[0]), float(this_data[1]))

                except (IndexError, ValueError):

                    self.winning_condition = 598
                    self.loosing_condition = 264
                    self.start_the_match()

            elif self.user in this_data:

                self.users = this_data
                self.users.remove(self.user)

            else:

                self.opponent = this_data[0]
                self.winning_condition = 264
                self.loosing_condition = 598
                self.start_the_match()

        def start_the_match(self):

            self.border_paths_list = []
            self.neighb_coords_x = []
            self.neighb_coords_y = []
            self.marked_spots_list = []
            self.marked_paths_list = []
            self.ball = Ball(self.points_coordinates_x[0],
                             self.points_coordinates_y[0])
            self.curr_spot = Spot(self.points_coordinates_x[0],
                                  self.points_coordinates_y[0])

            self.main_win = GraphWin('An undisclosed location', 700, 700)
            self.main_win.background = Image(Point(350, 350),
                                             "data/artwork/background.png")
            self.main_win.background.draw(self.main_win)
            self.main_win.foreground = Image(Point(705, 840),
                                             "data/artwork/foreground.png")
            self.main_win.foreground.draw(self.main_win)
            self.main_win.point_padding = 5

            self.ball_circle = Circle(Point(self.ball.pos_x, self.ball.pos_y),
                                      self.main_win.point_padding-2)
            self.ball_circle.draw(self.main_win)

            self.want_spot = Spot(self.ball.pos_x, self.ball.pos_y)
            self.want_spot.setM()
            self.marked_spots_list.append(self.want_spot)

            if self.player.acting:

                self.ball_circle.setFill(self.player.color)
                self.draw_pitch(self.player.color, 'red')
                self.make_move()

            else:

                self.ball_circle.setFill('red')
                self.draw_pitch('red', self.player.color)

        def draw_pitch(self, p_1_color, p_2_color):

            for bord_spot in self.border_spots:

                self.marked_spots_list.append(bord_spot)

                for spot_bord in self.border_spots:

                    if spot_bord.pos_x > bord_spot.pos_x-32\
                            and spot_bord.pos_x < bord_spot.pos_x+32\
                            and spot_bord.pos_y > bord_spot.pos_y-32\
                            and spot_bord.pos_y < bord_spot.pos_y+32:

                        bordPath = Path(bord_spot, spot_bord)

                        if bord_spot.pos_x < 264:
                            bordPath.draw(self.main_win, p_1_color)
                        elif bord_spot.pos_x > 598:
                            bordPath.draw(self.main_win, p_2_color)
                        else:
                            bordPath.draw(self.main_win, 'black')
                        self.border_paths_list.append(bordPath)

        def detect_neighbourhood(self):

            del self.neighb_coords_x[:]
            del self.neighb_coords_y[:]

            n = 0
            for coord_x in self.points_coordinates_x:

                coord_y = self.points_coordinates_y[n]

                if coord_x > self.curr_spot.pos_x-52\
                        and coord_x < self.curr_spot.pos_x+52\
                        and coord_y > self.curr_ball_pos_y-52\
                        and coord_y < self.curr_ball_pos_y+52\
                        and coord_x != self.curr_spot.pos_x:

                    self.neighb_coords_x.append(coord_x)
                    self.neighb_coords_y.append(coord_y)
                n += 1

            logger.debug(str(len(self.neighb_coords_x)) +
                         " neighbours detected.")

        def check_winner(self):

            if self.want_spot.pos_x > 598:
                if self.winning_condition == 598:
                    logger.info(self.player.name +
                                " wins!\nClick to exit..")
                    self.winner = self.player.name
                else:
                    logger.info(self.opponent +
                                " wins!\nClick to exit..")
                    self.winner = self.opponent
            elif self.want_spot.pos_x < 264:
                if self.winning_condition == 264:
                    logger.info(self.player.name +
                                " wins!\nClick to exit..")
                    self.winner = self.player.name
                else:
                    logger.info(self.opponent +
                                " wins!\nClick to exit..")
                    self.winner = self.opponent

        def move_ball(self, dest_x, dest_y):

            self.want_spot = Spot(dest_x, dest_y)
            self.want_path = Path(self.curr_spot, self.want_spot)
            self.want_path.setM()
            self.marked_paths_list.append(self.want_path)
            if self.player.acting:
                self.want_path.draw(self.main_win, self.player.color)
            else:
                self.want_path.draw(self.main_win, 'red')

            logger.debug("Marked path (" +
                         str(self.curr_spot.pos_x) + "," +
                         str(self.curr_spot.pos_y) + ") <--> (" +
                         str(self.want_spot.pos_x) + "," +
                         str(self.want_spot.pos_y) + ").")

            self.curr_spot = self.want_spot

            if self.want_spot not in self.marked_spots_list:

                self.want_spot.setM()
                self.marked_spots_list.append(self.want_spot)
                logger.debug("Marked spot (" +
                             str(self.want_spot.pos_x) + "," +
                             str(self.want_spot.pos_y) + ").")
                self.player.acting = not self.player.acting
                logger.debug('possesion changed')

            self.ball.pos_x = dest_x
            self.ball.pos_y = dest_y

            self.ball_circle.undraw()
            self.ball_circle = Circle(Point(self.ball.pos_x, self.ball.pos_y),
                                      self.main_win.point_padding-2)
            if self.player.acting:
                self.ball_circle.setFill(self.player.color)
            else:
                self.ball_circle.setFill('red')
            self.ball_circle.draw(self.main_win)

            if self.player.acting:
                reactor.callLater(0.125, self.make_move)

        def make_move(self):

            self.curr_spot.pos_x = self.ball.pos_x
            self.curr_ball_pos_y = self.ball.pos_y

            self.check_winner()
            try:
                self.transport.write('{}'.format(self.winner).encode())
                logger.debug('\'{}\' --> (\'{}\', {})'.format
                             (self.winner, self.transport.getPeer().host,
                              self.transport.getPeer().port))
                #return 0
            except AttributeError:
                pass

            self.detect_neighbourhood()

            curr_ball_x_pos = self.ball.pos_x
            while self.ball.pos_x == curr_ball_x_pos:

                try:

                    xyPoint = self.main_win.getMouse()

                except GraphicsError:

                    return 'Null'

                dest_x = xyPoint.getX()
                dest_y = xyPoint.getY()

                n = 0
                for coord_x in self.neighb_coords_x:

                    coord_y = self.neighb_coords_y[n]

                    if dest_x < self.curr_spot.pos_x-14\
                            or dest_x > self.curr_spot.pos_x+14:

                        if dest_x < coord_x+self.main_win.point_padding\
                                and dest_x > coord_x-self.main_win.point_padding\
                                and dest_y < coord_y+self.main_win.point_padding\
                                and dest_y > coord_y-self.main_win.point_padding:

                            self.want_spot = Spot(coord_x, coord_y)
                            logger.debug("Click near neighboring (" +
                                         str(self.want_spot.pos_x) + "," +
                                         str(self.want_spot.pos_y) + ").")

                            break

                        else:

                            self.want_spot = self.curr_spot

                    elif dest_y > self.curr_ball_pos_y+14 \
                            or dest_y < self.curr_ball_pos_y-14:

                        if dest_y > coord_y-self.main_win.point_padding\
                                and dest_y < coord_y+self.main_win.point_padding\
                                and dest_x < coord_x+self.main_win.point_padding\
                                and dest_x > coord_x-self.main_win.point_padding:

                            self.want_spot = Spot(coord_x, coord_y)
                            logger.debug("Click near neighboring (" +
                                         str(self.want_spot.pos_x) + "," +
                                         str(self.want_spot.pos_y) + ").")

                            break

                        else:

                            self.want_spot = self.curr_spot

                    else:

                        self.want_spot = self.curr_spot

                    n += 1

                self.want_path = Path(self.curr_spot, self.want_spot)

                if self.want_spot != self.curr_spot and\
                        self.want_path not in self.marked_paths_list and\
                        self.want_path not in self.border_paths_list:

                    self.move_ball(self.want_spot.pos_x,
                                   self.want_spot.pos_y)
                    self.transport.write('{} {}'.format
                                         (self.ball.pos_x,
                                          self.ball.pos_y).encode())
                    logger.debug('\'{} {}\' --> (\'{}\', {})'.format
                                 (self.ball.pos_x, self.ball.pos_y,
                                  self.transport.getPeer().host,
                                  self.transport.getPeer().port))

    class protocol_factory(Factory):

        def startedConnecting(self, connector):

            logger.debug('contacting (\'{}\', {})'.format(server_ip, port_no))

        def buildProtocol(self, addr):

            logger.info('connected')
            return protocol_instance(self)

        def clientConnectionLost(self, connector, reason):

            logger.info('disconnected')

        def clientConnectionFailed(self, connector, reason):

            logger.debug('connection failed')

    reactor.connectTCP(server_ip, port_no, protocol_factory())
    reactor.run()
