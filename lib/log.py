def define_logger(a_logfile, debug_mode):

    import logging

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    filelog = logging.FileHandler(a_logfile)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    filelog.setFormatter(formatter)
    filelog.setLevel(logging.DEBUG)

    streamlog = logging.StreamHandler()
    formatter = logging.Formatter('%(levelname)s %(message)s')
    streamlog.setFormatter(formatter)
    if debug_mode:
        streamlog.setLevel(logging.DEBUG)
    else:
        streamlog.setLevel(logging.INFO)

    logger.addHandler(filelog)
    logger.addHandler(streamlog)

    return logger
