def parse_cl_args():

    import argparse

    parser = argparse.ArgumentParser(description='Pootie - play an online game of Pootie against friends and enemies alike!')

    parser.add_argument("-m", "--mode", default='cli',
                        choices=['cli', 'gui', 'client', 'server'],
                        help="specify run mode, \'cli\' by default")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="enables debugging")

    return parser.parse_args()
