from lib.graphics import Circle, GraphWin, GraphicsError, Image, Line, Point
import shelve


class Ball:

    def __init__(self, pos_x=0, pos_y=0, color='black'):

        self.pos_x = pos_x
        self.pos_y = pos_y

    def __str__(self):

        return "\'Ball\' object at (" + str(self.pos_x) + "," + str(
                self.pos_y) + ")."

    def getX(self):

        return self.pos_x

    def getY(self):

        return self.pos_y

class Spot:

    def __init__(self, pos_x=0, pos_y=0):

        self.pos_x = pos_x
        self.pos_y = pos_y
        self.marked = False

    def __str__(self):

        return("\'Spot\' object at (" + str(self.getX()) + "," + str(
                self.getY()) + ").")

    def __eq__(self, other):

        if self.pos_x == other.pos_x and self.pos_y == other.pos_y:

            return True

        else:

            return False

    def draw(self, window, point_padding):

        spotCircle = Circle(Point(self.getX(), self.getY()), point_padding)
        spotCircle.draw(window)

    def getX(self):

        return self.pos_x

    def getY(self):

        return self.pos_y

    def setM(self):

        self.marked = True

    def getM(self):

        return self.marked


class Path:

    def __init__(self, path_start, path_end, width=2):

        self.path_start = path_start
        self.path_end = path_end
        self.width = width
        self.marked = False

    def __str__(self):

        return "\'Path\' object marking moves."

    def __eq__(self, other):

        if self.path_start == other.path_start \
                and self.path_end == other.path_end:
            return True

        elif self.path_start == other.path_end \
                and self.path_end == other.path_start:
            return True

        return False

    def setM(self):

        self.marked = True

    def getM(self):

        return self.marked

    def draw(self, window, color):

        # pathLine = Line(self.path_start, self.path_end)
        pathLine = Line(Point(self.path_start.getX(), self.path_start.getY()),
                        Point(self.path_end.getX(), self.path_end.getY()))
        pathLine.setWidth(self.width)
        pathLine.setOutline(color)
        pathLine.draw(window)


class Player:

    def __init__(self, name='Pootie Player', acting=False, color='black'):

        self.acting = acting
        self.color = color
        self.name = name

    def __str__(self):

        return("Player object \'self.name\'.")

    def getA(self):

        return self.active

    def setC(self, red=255, green=255, blue=255):

        self.color = color_rgb(red, green, blue)


def kickoff(logger, p_1, p_2, point_padding=5,
            stadium='An Undisclosed Location'):

    mainWin = GraphWin(stadium, 700, 700)

    background = Image(Point(350, 350), "data/artwork/background.png")
    background.draw(mainWin)
    foreground = Image(Point(705, 840), "data/artwork/foreground.png")
    foreground.draw(mainWin)

    points_coords_shelve = shelve.open("data/points_coords.db")
    points_coordinates_x = points_coords_shelve['points_x-coords']
    points_coordinates_y = points_coords_shelve['points_y-coords']
    points_coords_shelve.close()
    border_spots_shelve = shelve.open("data/border_spots.db")
    border_spots = border_spots_shelve['border_points']
    border_spots_shelve.close()

    neighb_coords_x = []
    neighb_coords_y = []
    marked_spots_list = []
    marked_paths_list = []
    ball = Ball(points_coordinates_x[0], points_coordinates_y[0])
    player_1 = Player(p_1, None, "green")
    player_2 = Player(p_2, True, "red")

    border_paths_list = []
    for bord_spot in border_spots:
        for spot_bord in border_spots:

            if spot_bord.getX() > bord_spot.getX()-32\
                    and spot_bord.getX() < bord_spot.getX()+32\
                    and spot_bord.getY() > bord_spot.getY()-32\
                    and spot_bord.getY() < bord_spot.getY()+32:

                bordPath = Path(bord_spot, spot_bord)
                if bord_spot.getX() < 264:
                    bordPath.draw(mainWin, player_1.color)
                elif bord_spot.getX() > 598:
                    bordPath.draw(mainWin, player_2.color)
                else:
                    bordPath.draw(mainWin, 'black')
                border_paths_list.append(bordPath)

    def detect_neighbourhood(logger, curr_ball_pos_x, curr_ball_pos_y):

        del neighb_coords_x[:]
        del neighb_coords_y[:]
        n = 0
        for coord_x in points_coordinates_x:
            coord_y = points_coordinates_y[n]
            if coord_x > curr_ball_pos_x-52 and coord_x < curr_ball_pos_x+52\
                    and coord_y > curr_ball_pos_y-52\
                    and coord_y < curr_ball_pos_y+52\
                    and coord_x != curr_ball_pos_x:
                neighb_coords_x.append(coord_x)
                neighb_coords_y.append(coord_y)
            n += 1
        logger.debug(str(len(neighb_coords_x)) + " neighbours detected.")

    def end_match(player):

        print(player.name + " wins the game!\nClick to exit..")
        mainWin.getMouse()
        return player

    while True:

        curr_ball_pos_x = ball.getX()
        curr_ball_pos_y = ball.getY()
        ballCircle = Circle(Point(curr_ball_pos_x, curr_ball_pos_y),
                            point_padding-2)
        ballCircle.draw(mainWin)
        if player_1.active or not marked_spots_list:
            ballCircle.setFill(player_1.color)
        else:
            ballCircle.setFill(player_2.color)

        # del neighb_coords_x[:]
        # del neighb_coords_y[:]

        if not marked_spots_list:

            player_1.active = not player_1.active
            player_2.active = not player_2.active

            print("-------------------------------------")
            print("Ball's position set to (" + str(
                    ball.getX()) + "," + str(ball.getY()) + ").")
            currSpot = Spot(curr_ball_pos_x, curr_ball_pos_y)
            currSpot.setM()
            marked_spots_list.append(currSpot)
            logger.debug("Marked spot at (" + str(
                    currSpot.getX()) + "," + str(currSpot.getY()) + ").")
            print("-------------------------------------")
            turnLapse = False

            for bord_spot in border_spots:
                bord_spot.setM()
                marked_spots_list.append(bord_spot)

            logger.debug("Added " + str(len(border_spots)) +
                         " border spots to the list of marked spots.")

        if turnLapse:
            if player_1.active:
                print(player_1.name + "'s turn.")
            else:
                print(player_2.name + "'s turn.")
            turnLapse = not turnLapse
        elif not marked_paths_list and player_1.active:
            print(player_1.name + "'s turn.")
        elif not marked_paths_list and player_2.active:
            print(player_2.name + "'s turn.")

        detect_neighbourhood(logger, curr_ball_pos_x, curr_ball_pos_y)

        curr_ball_x_pos = ball.getX()
        while ball.getX() == curr_ball_x_pos:

            try:
                xyPoint = mainWin.getMouse()
            except GraphicsError:
                return 'Null'
            dest_x = xyPoint.getX()
            dest_y = xyPoint.getY()

            n = 0
            for coord_x in neighb_coords_x:
                coord_y = neighb_coords_y[n]

                if dest_x < curr_ball_pos_x-14 or dest_x > curr_ball_pos_x+14:
                    if dest_x < coord_x+point_padding\
                            and dest_x > coord_x-point_padding\
                            and dest_y < coord_y+point_padding\
                            and dest_y > coord_y-point_padding:

                        wantSpot = Spot(coord_x, coord_y)
                        logger.debug("Detected click near neighboring (" + str(
                                wantSpot.getX()) + "," + str(
                                        wantSpot.getY()) + ").")
                        break

                    else:
                        wantSpot = currSpot

                elif dest_y > curr_ball_pos_y+14 \
                        or dest_y < curr_ball_pos_y-14:
                    if dest_y > coord_y-point_padding\
                            and dest_y < coord_y+point_padding\
                            and dest_x < coord_x+point_padding\
                            and dest_x > coord_x-point_padding:

                        wantSpot = Spot(coord_x, coord_y)
                        logger.debug("Detected click near neighboring (" + str(
                                wantSpot.getX()) + "," + str(
                                        wantSpot.getY()) + ").")
                        break

                    else:
                        wantSpot = currSpot

                else:
                    wantSpot = currSpot

                n += 1

            wantPath = Path(currSpot, wantSpot)

            if wantSpot != currSpot:

                if not marked_paths_list:

                    wantPath.setM()
                    marked_paths_list.append(wantPath)
                    if player_1.active:
                        wantPath.draw(mainWin, player_1.color)
                    else:
                        wantPath.draw(mainWin, player_2.color)
                    logger.debug("Marked path between (" + str(
                            currSpot.getX()) + "," + str(
                                    currSpot.getY()) + ") and (" + str(
                                            wantSpot.getX()) + "," + str(
                                                    wantSpot.getY()) + ").")

                    wantSpot.setM()
                    marked_spots_list.append(wantSpot)
                    logger.debug("Marked spot at (" + str(
                            wantSpot.getX()) + "," + str(
                                    wantSpot.getY()) + ").")
                    currSpot = wantSpot

                    ballCircle.undraw()
                    new_ball_pos_x = currSpot.getX()
                    new_ball_pos_y = currSpot.getY()
                    ball.kick(new_ball_pos_x, new_ball_pos_y)
                    logger.debug("New ball position: (" + str(
                            ball.getX()) + "," + str(ball.getY()) + ").")

                    for bord_path in border_paths_list:
                        bord_path.setM()
                        marked_paths_list.append(bord_path)

                    player_1.active = not player_1.active
                    player_2.active = not player_2.active
                    print("-------------------------------------")
                    turnLapse = True

                elif wantPath not in marked_paths_list:

                    wantPath.setM()
                    marked_paths_list.append(wantPath)
                    if player_1.active:
                        wantPath.draw(mainWin, player_1.color)
                    else:
                        wantPath.draw(mainWin, player_2.color)
                    logger.debug("Marked path between (" + str(
                            currSpot.getX()) + "," + str(
                                    currSpot.getY()) + ") and (" + str(
                                            wantSpot.getX()) + "," + str(
                                                    wantSpot.getY()) + ").")

                    currSpot = wantSpot

                    ballCircle.undraw()

                    new_ball_pos_x = currSpot.getX()
                    new_ball_pos_y = currSpot.getY()
                    ball.kick(new_ball_pos_x, new_ball_pos_y)
                    logger.debug("New ball position: (" + str(
                            ball.getX()) + "," + str(ball.getY()) + ").")

                    if wantSpot.pos_x > 598:

                        end_match(player_1)

                    elif wantSpot.pos_x < 264:

                        end_match(player_2)

                    elif wantSpot not in marked_spots_list:

                        wantSpot.setM()
                        marked_spots_list.append(wantSpot)
                        logger.debug("Marked spot at (" + str(
                                     wantSpot.getX()) + "," + str(
                                            wantSpot.getY()) + ").")

                        player_1.active = not player_1.active
                        player_2.active = not player_2.active
                        turnLapse = True

                        print("-------------------------------------")


def createPointGrid(no_of_points):

    points_coords_shelve = shelve.open("data/points_coords.db")

    try:

        points_coordinates_x = points_coords_shelve['points_x-coords']
        points_coordinates_y = points_coords_shelve['points_y-coords']

    except:

        points_coordinates_x = []
        points_coordinates_y = []

    n = 0
    for coord_x in points_coordinates_x:

        coord_y = points_coordinates_y[n]
        xyPoint = Point(coord_x, coord_y)
        print(xyPoint)
        xyCircle = Circle(xyPoint, 3)
        xyCircle.draw(mainWin)

        n += 1
        no_of_points -= 1

    while no_of_points > 0:

        xyPoint = mainWin.getMouse()
        xyCircle = Circle(xyPoint, 3)
        xyCircle.draw(mainWin)
        print(xyPoint)
        points_coordinates_x.append(xyPoint.getX())
        points_coordinates_y.append(xyPoint.getY())

        no_of_points -= 1

    points_coords_shelve['points_x-coords'] = points_coordinates_x
    points_coords_shelve['points_y-coords'] = points_coordinates_y
    points_coords_shelve.close()

    print("No of points in the grid:" + str(len(points_coordinates_x)) + ".")

    border_points = []
    bord_neighb_coords_x = []
    m = 0
    for x_coord in points_coordinates_x:
        y_coord = points_coordinates_y[m]
        n = 0

        for coord_x in points_coordinates_x:
            coord_y = points_coordinates_y[n]

            if coord_x > x_coord-52 and coord_x < x_coord+52 \
                    and coord_y > y_coord-52 and coord_y < y_coord+52\
                    and x_coord != coord_x:

                bord_neighb_coords_x.append(coord_x)

            n += 1

        if len(bord_neighb_coords_x) < 8:

            xySpot = Spot(x_coord, y_coord)
            border_points.append(xySpot)

        del bord_neighb_coords_x[:]
        m += 1

    border_points_shelve = shelve.open("data/border_spots.db")
    border_points_shelve['border_points'] = border_points
    print("Added " + str(len(border_points)) +
          " entries to border-points database.")
    border_points_shelve.close()

    mid_point = Point(points_coordinates_x[0], points_coordinates_y[0])
    testCircle = Circle(mid_point, 52)
    testCircle.draw(mainWin)
    ndTestCircle = Circle(mid_point, 33)
    ndTestCircle.draw(mainWin)
    testLine = Line(Point(264, 0), Point(264, 800))
    testLine.draw(mainWin)
    testLine2 = Line(Point(598, 0), Point(598, 800))
    testLine2.draw(mainWin)

    print("Complete, click to exit.")
    mainWin.getMouse()
