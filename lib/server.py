def server(logger, port_no=14507):

    from twisted.internet.protocol import Factory, Protocol
    from twisted.internet import reactor

    class protocol_instance(Protocol):

        def __init__(self, factory):

            self.factory = factory
            self.opponent = None
            self.user = None

        def connectionMade(self):

            logger.debug('{} connect'.format(self.transport.client))

        def connectionLost(self, reason):

            if self.user in self.factory.clients:

                del self.factory.clients[self.user]
                logger.info('\'{}\' disconnect'.format(self.user))
                self.send_client_list()

            else:

                logger.debug('{} disconnect'.format(self.transport.client))

        def dataReceived(self, data):

            logger.debug('{} --> \'{}\''.format(self.transport.client, data))
            data_as_list = data.decode().split()

            if self.opponent:

                self.opponent.transport.write(data)
                logger.debug('(\'{}\', {}) --> \'{}\' --> (\'{}\', {})'.format
                             (self.transport.getPeer().host,
                              self.transport.getPeer().port,
                              data,
                              self.opponent.transport.getPeer().host,
                              self.opponent.transport.getPeer().port))

            elif self.user and data_as_list[0] in self.factory.clients:

                self.opponent = self.factory.clients[data_as_list[0]]
                self.factory.clients[data_as_list[0]].opponent =\
                    self.factory.clients[self.user]
                self.opponent.transport.write(self.user.encode())
                logger.debug('\'{}\' --> (\'{}\', {})'.format(self.user,
                             self.opponent.transport.getPeer().host,
                              self.opponent.transport.getPeer().port))
                self.transport.write(self.opponent.user.encode())
                logger.debug('\'{}\' --> (\'{}\', {})'.format
                             (self.opponent.user,
                              self.transport.getPeer().host,
                              self.transport.getPeer().port))

            else:

                self.user = data_as_list[0]
                self.factory.clients[self.user] = self
                logger.info('\'{}\' connect'.format
                            (self.factory.clients[self.user].user))
                self.send_client_list()

        def send_client_list(self):

            for name, protocol in self.factory.clients.items():

                protocol.transport.write('{}'.format
                                         (' '.join([*self.factory.
                                          clients])).encode())
                logger.debug('\'{}\' --> (\'{}\', {})'.format
                             ([*self.factory.clients],
                              protocol.transport.getPeer().host,
                              protocol.transport.getPeer().port))

    class protocol_factory(Factory):

        def __init__(self):

            self.protocols = []
            self.clients = {}

        def buildProtocol(self, addr):

            if not len(self.clients) > 2:

                return protocol_instance(self)

    reactor.listenTCP(port_no, protocol_factory())
    logger.info('listening on {}'.format(port_no))

    reactor.run()
