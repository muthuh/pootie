def cli(a_logger):

    from lib.constants import WEL_MSG, BYE_MSG
    from lib.io import print_surround, validate_input, validate_char_response
    from lib.misc import call_procedure

    print_surround(WEL_MSG)

    while True:

        user_response = set(validate_input
                            ('OPTIONS (C)lient (S)erver (E)xit ', str))
        user_response = [each_of.lower() for each_of in user_response]

        if validate_char_response(user_response, ['c', 's', 'e']):

            if 'c' in user_response:

                from lib.rc import port_number, server_ip
                from lib.client import client

                call_procedure(a_logger, 'client', client,
                               [a_logger, server_ip, port_number], True, True)

            elif 's' in user_response:

                from lib.rc import port_number
                from lib.server import server

                call_procedure(a_logger, 'server', server,
                               [a_logger, port_number], True, True)

            if 'e' in user_response:

                break

    print_surround(BYE_MSG)


def gui(a_logger):

    from pylibui.core import App
    from pylibui.controls import Window

    class MyWindow(Window):

        def onClose(self, data):
            super().onClose(data)
            app.stop()

    app = App()

    window = MyWindow('Home Utilities Interconnected', 400, 300)
    window.setMargined(True)
    window.show()

    app.start()
    app.close()
