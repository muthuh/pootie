def call_procedure(a_logger, func_title, a_func, args=[],
                   subprocess_flag=False, quiet_flag=False):

    from lib.io import print_surround

    if quiet_flag:
        print_surround(func_title.upper(), False, True, False)

    a_logger.debug('calling the \'{}\' procedure'.format(func_title))

    if subprocess_flag:

        exit_code = a_func(*args)

    else:

        exit_code = a_func(*args)

    a_logger.debug('the \'{}\' procedure returned \'{}\''.format
                   (func_title, exit_code))
    if quiet_flag:
        for _ in range(len(func_title)):
            print(u'\u02D9', end='')
        print('')

    return exit_code


def decision(probability=50):

    from random import randrange

    return randrange(100) < probability
