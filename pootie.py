#!/usr/bin/env python
from lib.constants import NAME
from lib.args import parse_cl_args
from lib.log import define_logger
from lib.misc import call_procedure


def main(main_logger, cl_args):

    if cl_args.mode == 'cli':

        from lib.ints import cli

        call_procedure(main_logger, 'cli', cli, [main_logger])

    if cl_args.mode == 'gui':

        from lib.ints import gui

        call_procedure(main_logger, 'gui', gui, [main_logger], True)

    if cl_args.mode == 'server':

        from lib.rc import port_number
        from lib.server import server

        call_procedure(main_logger, 'server', server,
                       [main_logger, port_number], True, True)

    if cl_args.mode == 'client':

        from lib.rc import port_number, server_ip
        from lib.server import client

        call_procedure(main_logger, 'client', client,
                       [main_logger, server_ip, port_number], True, True)


if __name__ == "__main__":

    cl_args = parse_cl_args()
    main_logger = define_logger('/tmp/{}.log'.format(NAME), cl_args.debug)

    try:

        main_logger.debug('~~ START ~~')

        main(main_logger, cl_args)

    except KeyboardInterrupt:

        main_logger.debug('keyboard interrupt')
        print('')

    finally:

        main_logger.debug('~~ EXIT ~~')
